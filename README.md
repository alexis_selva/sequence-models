These are the programming assignments relative to Deep Learning - Sequence models (5th part):

- Assignment 1.1: Building a recurrent neural network - step by step
- Assignment 1.2: Dinosaur Island - Character-Level Language Modeling
- Assignment 1.3: Jazz improvisation with LSTM
- Assignment 2.1: Operations on word vectors - Debiasing
- Assignment 2.2: Emojify
- Assignment 3.1: Neural Machine Translation with Attention
- Assignment 3.2: Trigger word detection

For more information, I invite you to have a look at https://www.coursera.org/learn/sequence-models
